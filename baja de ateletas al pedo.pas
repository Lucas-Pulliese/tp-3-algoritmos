procedure BAJA;
var
   opcC : Char;
   denei, isPos: Integer;
begin
     MOSTRARATL;
     delay(1500);
     repeat
           writeln('Ingrese cual de las siguientes bajas desea realizar: ');
           writeln('                                                    A - Atletas');
           writeln('                                                    D - Disciplina');
           writeln('                                                    P - Participantes');
           writeln('                                                    S - Sedes');
           writeln('                                                    M - Medallas');
           writeln('                                                    Q - Quit');
           readln(opcC);
     until (opcC <> 'A') and (opcC <> 'D') or (opcC <> 'P') or (opcC <> 'S') or (opcC <> 'M') or (opcC <> 'Q');
     // baja por atleta buscando por dni
     if (opcC = 'A') then
     begin
          writeln('Ingrese DNI del atleta que desea eliminar;');
          readln(denei);
          assign(ATLS,'atletas.dat');
          rewrite(ATLS);
          reset(ATLS);
          read(ATLS,atleta);
          writeln('este es el primero ',atleta.dni,' ',atleta.nombre);
          while(not(EOF(ATLS)) and (atleta.dni <> denei)) do
               read(ATLS,atleta);
          writeln(atleta.dni);
          delay(1500);
          if (atleta.dni = denei) then  //si lo encuentro ya estoy en la posicion siguiente
          begin
               isPos :=  filepos(ATLS);
               while (not(EOF(ATLS))) do
               begin
                   read(ATLS, atleta);
                   seek(ATLS,filepos(ATLS)-2);
                   write(ATLS,atleta);
                   seek(ATLS,filepos(ATLS)+1);
               end;
               seek(ATLS, isPos-1);
               read(ATLS,atleta);
               writeln(atleta.nombre);
          end;
          close(ATLS);
          MOSTRARATL;
          delay(1500);
     end;
end;
