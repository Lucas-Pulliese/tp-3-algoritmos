//Lucas Pulliese   Martín Ballestero   Comisión:114
program tp2(input,output);
uses crt, sysutils;
type
    ATL = record
        nombre: String;
        dnidentidad: Longint;
    end;
    DIS = record
        codDis, descr, descripto : String;
        equipo: Char;
    end;
    PAR = record
        disCod : String;
        anio, doc : Integer;
    end;
    SED = record
        codInt, descript : String;
    end;
    MED = record
        docID,year: Integer;
        intCod : String;
        medallas : array [1..3] of Integer;
    end;
var
    opc:Integer;
    atleta: ATL;
    disciplina: DIS;
    participante: PAR;
    sede: SED;
    medalla: MED;
    ATLS: File of ATL;
    DISS: File of DIS;
    PARS: File of PAR;
    SEDS: File of SED;
    MEDS: File of MED;

procedure MOSTRARATL;
begin
      assign(ATLS,'atletas.dat');
      reset(ATLS);
      while(not(EOF(ATLS))) do
      begin
           read(ATLS,atleta);
           write(atleta.nombre);
           writeln(' ',atleta.dnidentidad);
      end;
      close(ATLS);
end;

procedure MOSTRARDISC;
begin
      assign(DISS,'disciplinas.dat');
      reset(DISS);
      while(not(EOF(DISS))) do
      begin
           read(DISS,disciplina);
           write(disciplina.codDis);
           write(' ',disciplina.descr);
           write(' ',disciplina.equipo);
      end;
      close(DISS);
end;

procedure MOSTRARPAR;
begin
      assign(PARS,'participantes.dat');
      reset(PARS);
      while(not(EOF(PARS))) do
      begin
           read(PARS,participante);
           write(participante.disCod);
           write(' ',participante.anio);
           writeln(' ',participante.doc);
      end;
      close(PARS);
end;

procedure MOSTRARSED;
begin
      assign(SEDS,'sedes.dat');
      reset(SEDS);
      while(not(EOF(SEDS))) do
      begin
           read(SEDS,sede);
           write(sede.codInt);
           writeln(' ',sede.descript);
      end;
      close(SEDS);
end;

procedure MOSTRARMED;
var
    i:Integer;
begin
      assign(MEDS,'medallasxatleta.dat');
      reset(MEDS);
      while(not(EOF(MEDS))) do
      begin
           read(MEDS,medalla);
           write(medalla.intCod);
           for i:=1 to 3 do
                writeln(' ',medalla.medallas[i]);
      end;
      close(MEDS);
end;

procedure MOSTRAR;
var
    num: Integer;
    rta: Char;
begin
    repeat
        repeat
            writeln('Ingrese cual de los 5 archivos desea visualizar:');
            writeln('                                           1-Atletas.');
            writeln('                                           2-Disciplinas.');
            writeln('                                           3-Participantes.');
            writeln('                                           4-Sedes.');
            writeln('                                           5-Medallas por atletas.');
            readln(num);
            case num of
                1:MOSTRARATL;
                2:MOSTRARDISC;
                3:MOSTRARPAR;
                4:MOSTRARSED;
                5:MOSTRARMED;
            end;
            writeln('Desea ver otro archivo? <N> / <S>');
            readln(rta);
            ClrScr;
        until ((rta = 'N') or (rta = 'S'));
    until ((rta = 'N'));
end;

procedure ALTA;
var
    opc: Integer;
    rta: Char;
begin
    repeat
        writeln('ALTA DE LOS REGISTROS');
        writeln();
        writeln('Elija entre las siguientes opciones: 1- Atletas');
        writeln('                                     2- Disciplinas');
        writeln('                                     3- Participantes');
        writeln('                                     4- Sedes');
        writeln('                                     5- Medallas por atleta');
        readln(opc);
    until (opc >0) and (opc <6);
    if (opc = 1) then   //alta para atletas
       begin
            assign(ATLS,'atletas.dat');
            reset(ATLS);
            seek(ATLS,filesize(ATLS));
            writeln();
            repeat
                  writeln('Ingrese DNI del atleta a ingresar:');
                  readln(atleta.dnidentidad);
                  writeln('Ingrese nombre del atleta a ingresar:');
                  readln(atleta.nombre);
                  write(ATLS,atleta);   //grabo en archivo
                  writeln('Desea ingresas un nuevo atleta? <N> / <S>');
                  readln(rta);
                  ClrScr;
            until ((rta = 'N'));
            close(ATLS);
       end;
    if (opc = 2) then   //alta para disciplinas
       begin
            assign(DISS,'disciplinas.dat');
            reset(DISS);
            seek(DISS,filesize(DISS));
            repeat
                  writeln('Ingrese si la disciplina a ingresar es en grupo o no: <N> o <S>');
                  readln(disciplina.equipo);
                   writeln('Ingrese codigo de la disciplina a ingresar:');
                   readln(disciplina.codDis);
                   writeln('Ingrese descripcion de la disciplina a ingresar:');
                   readln(disciplina.descr);
                   write(DISS,disciplina);   //grabo en archivo
                   writeln('Desea ingresas una nueva disciplina? <N> / <S>');
                  readln(rta);
                  ClrScr;
            until ((rta = 'N'));
            close(DISS);
       end;
    if (opc = 3) then   //alta para participantes
       begin
            assign(PARS,'participantes.dat');
            reset(PARS);
            seek(PARS,filesize(PARS));
            repeat
                  writeln('Ingrese año en el que se participó:');
                  readln(participante.anio);
                   writeln('Ingrese codigo de la disciplina a ingresar:');
                   readln(participante.disCod);
                   writeln('Ingrese DNI del participante:');
                   readln(participante.doc);
                   write(PARS,participante);   //grabo en archivo
                   writeln('Desea ingresas un nuevo participante? <N> / <S>');
                  readln(rta);
                  ClrScr;
            until ((rta = 'N'));
            seek(PARS,filesize(PARS));
            write(PARS,participante);
            delay(1000);
            close(PARS);
       end;
       if (opc = 4) then   //alta para sedes
       begin
            assign(SEDS,'sedes.dat');
            reset(SEDS);
            seek(SEDS,filesize(SEDS));
            repeat
                  writeln('Ingrese codigo internacional de la sede:');
                  readln(sede.codInt);
                  writeln('Ingrese descripcion del codigo internacional:');
                  readln(sede.descript);
                  write(SEDS,sede);   //grabo en archivo
                  writeln('Desea ingresas una nueva sede? <N> / <S>');
                  readln(rta);
                  ClrScr;
            until ((rta = 'N'));
            seek(SEDS,filesize(SEDS));
            write(SEDS,sede);
            delay(1000);
            close(SEDS);
       end;
       if (opc = 5) then   //alta para medallas por atletas
       begin
            assign(MEDS,'medallasxatleta.dat');
            reset(MEDS);
            seek(MEDS,filesize(MEDS));
            repeat
                  writeln('Ingrese codigo internacional de la sede:');
                  readln(medalla.intCod);
                  writeln('Ingrese a�o de la competencia:');
                  readln(medalla.year);
                  writeln('Ingrese DNI del atleta:');
                  readln(medalla.docID);
                  writeln('Ingrese medallas de oro:');
                  readln(medalla.medallas[1]);
                  writeln('Ingrese medallas de plata:');
                  readln(medalla.medallas[2]);
                  writeln('Ingrese medallas de bronce:');
                  readln(medalla.medallas[3]);
                  write(MEDS,medalla);   //grabo en archivo
                  writeln('Desea ingresar una nueva medalla por atleta? <N> / <S>');
                  readln(rta);
                  ClrScr;
            until ((rta = 'N'));
            seek(MEDS,filesize(MEDS));
            write(MEDS,medalla);
            delay(1000);
            close(MEDS);
       end;
end;

procedure BAJA;
var
   opcC : Char;
   sedecod: String;
   isPos: Integer;
begin
     // baja de sede
      MOSTRARSED;
      delay(1500);
      writeln('Ingrese sede que desea eliminar por codigo internacional:');
      readln(sedecod);
      assign(SEDS,'sedes.dat');
      reset(SEDS);
      read(SEDS,sede);
      while(not(EOF(SEDS)) and (CompareStr(sede.codInt,sedecod) <> 0)) do
           read(SEDS,sede);
      if (CompareStr(sede.codInt,sedecod) = 0) then  //si lo encuentro ya estoy en la posicion siguiente
      begin
           seek(SEDS, filepos(SEDS)-1);
           sede.codInt := '-';
           sede.descript := '-';
           write(SEDS,sede);
      end;
      close(SEDS);
      MOSTRARSED;
      delay(1500);
end;

procedure MODIFICACION;
var
   sedecod, newcod, newdesc: String;
   busrta: Integer;
begin
      MOSTRARSED;
      delay(1500);
      repeat
        writeln('Desea buscar la sede a modificar con el codigo internacional <1> o la descripcion <2> ? ');
        readln(busrta);
      until (busrta = 1) or (busrta = 2);
      if (busrta = 1) then
      begin
          writeln('Ingrese sede que desea modificar por codigo internacional:');
      end
      else
      begin
          writeln('Ingrese sede que desea modificar por descripcion:');
      end;
      readln(sedecod);
      assign(SEDS,'sedes.dat');
      reset(SEDS);
      read(SEDS,sede);
      while(not(EOF(SEDS)) and (CompareStr(sede.codInt,sedecod) <> 0) and (CompareStr(sede.descript,sedecod) <> 0)) do
           read(SEDS,sede);
      if ((CompareStr(sede.codInt,sedecod) <> 0) or (CompareStr(sede.descript,sedecod) <> 0)) then  //si lo encuentro ya estoy en la posicion siguiente
      begin
           seek(SEDS, filepos(SEDS)-1);
           writeln('Ingrese nueva descripcion de la sede:');
           readln(newdesc);
           writeln('Ingrese nuevo codigo internacional de la sede:');
           readln(newcod);
           sede.descript := newdesc;
           sede.codInt := newcod;
           write(SEDS,sede);
      end;
      close(SEDS);
      MOSTRARSED;
      delay(1500);
end;


procedure ABM;
var
    opc: Char;
begin
    repeat
        writeln('ALTERACION DE LOS REGISTROS');
        writeln();
        writeln('Elija entre las siguientes opciones: <A> , <B> o <M>');
        readln(opc);
    until (opc = 'A') or (opc = 'B') or (opc = 'M');
    case opc of
         'A': ALTA;
         'B': BAJA;
         'M': MODIFICACION;
    end;
end;

function BUSCAxDES(dis:String):String; //devuelve el codigo internacional dada la descripcion
var
    found: Integer;
begin
    assign(DISS,'disciplinas.dat');
    reset(DISS);
    found := 0;
    while(not(EOF(DISS)) and (found = 0)) do
    begin
        read(DISS,disciplina);
        if(CompareStr(disciplina.descr,dis) = 0) then
        begin
            BUSCAxDES := disciplina.codDis;
            found := 1;
        end;
    end;
    if (found = 0) then  //si no encontro la descripcion de ese codigo
        BUSCAxDES := '';
    close(DISS);
end;

function BUSCANAMExDNI(documento:Integer):String; //Busqueda dicotomica de Atletas por dni
var
    inf, sup, med: Integer;
begin
    assign(ATLS,'atletas.dat');
    inf := 0;
    med := filesize(ATLS) div 2;
    sup := filesize(ATLS);
    seek(ATLS,med);
    read(ATLS,atleta);
    while((documento <> atleta.dnidentidad) and (inf <= sup)) do
    begin
        if(documento > atleta.dnidentidad) then
        begin
            sup := med - 1;
        end
        else
        begin
            inf := med + 1;
        end;
        med := (inf+sup) div 2;
        seek(ATLS,med);
        read(ATLS,atleta);
    end;
    if(documento = atleta.dnidentidad) then
    begin
        seek(ATLS, filepos(ATLS)-1);
        read(ATLS,atleta);
        BUSCANAMExDNI := atleta.nombre;
    end
    else
        BUSCANAMExDNI := '';
    begin
    end;
    close(ATLS);
end;

procedure ATLXDIS;
var
    disciplina, codigoInt: String;
    anio: Integer;
begin
    writeln('EXHIBICION DE PARTICIPANTES DADA LA DISCIPLINA Y EL ANIO DE UNA COMPETENCIA');
    writeln('');
    writeln('Ingrese la disciplina por descripcion: ');
    readln(disciplina);
    writeln('Ingrese el anio de la competencia: <YYYY>');
    readln(anio);
    codigoInt := BUSCAxDES(disciplina);
    writeln(codigoInt);
    assign(PARS,'participantes.dat');
    reset(PARS);
    while (not(EOF(PARS))) do
    begin
        read(PARS, participante);
        if((participante.anio = anio) and (CompareStr(participante.disCod,codigoInt) = 0)) then
        begin
            writeln(BUSCANAMExDNI(participante.doc));
        end;
    end;
    close(PARS);
end;

// Cantidad de atletas
function cantAtletas(anio:integer):integer;
var 
    contador: Integer;
begin
    contador := 0;
    reset(PARS);
    while(not(EOF(PARS))) do
    begin
        read(PARS,participante);
        if(participante.anio = anio) then // Si es el mismo a�o sumo uno al contador
            begin
                contador := contador + 1;
            end;
    end;
    close(PARS);
    cantAtletas := contador;
end;

// Devolver descripcion de disciplina
procedure BUSCADISC(codigoDisc:string);
begin
    assign(DISS,'disciplinas.dat');
    reset(DISS);
    while(not(EOF(DISS))) do
    begin
        read(DISS,disciplina);
        if( (disciplina.codDis = codigoDisc) and (disciplina.descripto = '-') ) then // Si es el mismo codigo y no lo mostre antes
            begin
                writeln(disciplina.descr);
                disciplina.descripto := 'si';
                write(DISS,disciplina);
            end;
    end;
    close(DISS);
end;

// Devolver descripcion de disciplina
function BUSCADISCxCOD(codigoDisc:string):string;
var
    encontrado: string;
begin
    encontrado := '';
    assign(DISS,'disciplinas.dat');
    reset(DISS);
    while( (not(EOF(DISS))) and  (encontrado = '')) do
    begin
        read(DISS,disciplina);
        if( (disciplina.codDis = codigoDisc)) then 
            begin
                BUSCADISCxCOD := disciplina.descr;
                encontrado := 'si';
            end;
    end;
    close(DISS);
end;

procedure ATLXANO;
    var
    opc: Char;
    resultado, anio: Integer;
begin
    repeat
        repeat
            writeln('CANTIDAD DE ATLETAS POR A�O:');
            writeln();
            writeln('Ingrese el a�o en el que desea conocer la cantidad de atletas:');
            readln(anio);
        until (anio > 0) and (anio < 10000); // Me fijo que ingrese un a�o acorde
        resultado := cantAtletas(anio); // Cuento la cantidad atletas con una funcion
        if(resultado = 0) then // Checkeo si existio algun atleta en el a�o ingresado
            begin
                writeln('Ingreso un a�o en el que no hubo una olimpiada');
            end
            else 
            begin
                writeln('La cantida de atletas en el a�o ', anio, ' fueron: ', resultado);
            end;
        repeat
            writeln('Quiere seguir buscando? (S/N)');
            readln(opc);
        until (opc = 'S') or (opc = 'N');
    until (opc = 'N');
end;

procedure MEDXANO;
    var
    opc: Char;
    resultado, contador, anio, cantOro, cantPlata, cantBronce: Integer;
begin
    repeat
        repeat
            writeln('CANTIDAD DE MEDALLAS DE CADA TIPO POR A�O:');
            writeln();
            writeln('Ingrese el a�o en el que desea conocer la cantidad de medallas ganadas de cada tipo:');
            readln(anio);
        until (anio > 0) and (anio < 10000);
        contador := 0;
        cantOro := 0;
        cantPlata := 0;
        cantBronce := 0;
        reset(MEDS); // Abro el archivo
        while(not(EOF(MEDS))) do 
        begin
            read(MEDS,medalla);
            if(medalla.year = anio) then
                begin
                     if(medalla.medallas[1] > 0) then // Checkeo si hay alguna medalla
                        begin
                             cantOro := cantOro + medalla.medallas[1];
                        end;
                    if(medalla.medallas[2] > 0) then // Checkeo si hay alguna medalla
                        begin
                             cantPlata := cantPlata + medalla.medallas[2];
                        end;
                    if(medalla.medallas[3] > 0) then // Checkeo si hay alguna medalla
                        begin
                             cantBronce := cantBronce + medalla.medallas[3];
                        end;
                end;
        end;
        close(MEDS); // Cierro el archivo
        if((cantOro = 0) or (cantPlata = 0) or (cantBronce = 0)) then // Checkeo si el a�o fue correcto o existe
            begin
                writeln('Ingreso un a�o en el que no hubo una olimpiada o un a�o incorrecto');
            end
            else 
            begin
                writeln('La cantida de medallas en el a�o ', anio, ' fueron: oro:', cantOro, ', plata: ', cantPlata, ', bronce: ', cantBronce);
            end;
        repeat
            writeln('Quiere seguir buscando? (S/N)');
            readln(opc);
        until (opc = 'S') or (opc = 'N');
    until (opc = 'N');
end;

procedure SETEODISC;
begin
      assign(DISS,'disciplinas.dat');
      reset(DISS);
      while(not(EOF(DISS))) do
      begin
            read(DISS,disciplina);
            disciplina.descripto := '-';
            write(DISS,disciplina);
      end;
      close(DISS);
end;


procedure DISXANO;
var
    opc: Char;
    anio: integer;
begin
    repeat
        repeat
            writeln('DISCIPLINAS EN LAS QUE SE PARTICIPARON EN UN ANIO DETERMINADO:');
            writeln();
            writeln('Ingrese el a�o en el que desea conocer las disciplinas que se participaron:');
            readln(anio);
        until (anio > 0) and (anio < 10000);
        SETEODISC;
        assign(PARS,'participantes.dat');
        reset(PARS); // Abro el archivo
        while(not(EOF(PARS))) do 
        begin
            read(PARS,participante);
            if(participante.anio = anio) then
            begin
                BUSCADISC(participante.disCod);
            end;
        end;
        close(PARS); // Cierro el archivo
        repeat
            writeln('Quiere seguir buscando? (S/N)');
            readln(opc);
        until (opc = 'S') or (opc = 'N');
    until (opc = 'N');
end;

procedure TRAXATL;
var
    opc: Char;
    nombre: string;
    cantOro, cantPlata, cantBronce: Integer;
    dni: Longint;
begin
    repeat
        repeat
            writeln('TRAYECTORIA POR ATLETA:');
            writeln();
            writeln('Ingrese el DNI del atleta que desea conocer la trayectoria:');
            readln(dni);
        until (dni > 0) and (dni < 100000000);
        nombre := BUSCANAMExDNI(dni);
        if(nombre = '') then 
        begin
            writeln('No existe atleta con ese DNI');
        end
        else 
        begin
            writeln('Se mostrara el recorrido de:', nombre);
            assign(PARS,'participantes.dat');
            reset(PARS); // Primer lugar archivo
            while(not(EOF(PARS))) do // Trayectoria
            begin
                read(PARS,participante);
                if(participante.doc = dni) then
                begin
                    writeln('En el anio', participante.anio, ' participo de la disciplina ', BUSCADISCxCOD(participante.disCod));
                end;
            end;
            close(PARS);

            writeln('');
            writeln('-------------');
            writeln('-------------');
            writeln('');

            // Medallas ganadas
            reset(PARS); // Coloco cursor primer lugar archivo
            while(not(EOF(PARS))) do
            begin
                read(PARS,participante);
                if(participante.doc = dni) then
                begin
                    writeln('En el anio: ', participante.anio, ' gano las siguientes medallas:');
                    reset(MEDS); // Abro el archivo
                    while(not(EOF(MEDS))) do 
                    begin
                        read(MEDS,medalla);
                        if( (medalla.year = participante.anio) and (participante.doc = medalla.docID) ) then
                        begin
                            if(medalla.medallas[1] > 0) then // Checkeo si hay alguna medalla
                            begin
                                 cantOro := cantOro + medalla.medallas[1];
                            end;
                            if(medalla.medallas[2] > 0) then // Checkeo si hay alguna medalla
                            begin
                                 cantPlata := cantPlata + medalla.medallas[2];
                            end;
                            if(medalla.medallas[3] > 0) then // Checkeo si hay alguna medalla
                            begin
                                 cantBronce := cantBronce + medalla.medallas[3];
                            end;
                        end;
                    end;
                    writeln('Medallas de oro: ', cantOro, ', medallas de plata: ', cantPlata, ', medallas de bronce: ', cantBronce);
                end;
            end;

        end;
        repeat
            writeln('Quiere seguir buscando? (S/N)');
            readln(opc);
        until (opc = 'S') or (opc = 'N');
    until (opc = 'N');
end;

procedure PAIXMED;
var
    sedeMayor: string;
    medallasTemp, contador, medallasMayor, currentAnio, anioMayor: Integer;
begin 
    assign(MEDS,'medallasxatleta.dat');
    reset(MEDS); // Abro el archivo
    sedeMayor := '';
    contador := 0;
    medallasTemp := 0;
    medallasMayor := 0;
    currentAnio := 0;
    anioMayor := 0;
    while(not(EOF(MEDS))) do 
    begin
        read(MEDS,medalla);
        // Guardo los primeros datos
        if( contador = 0 ) then
        begin
            anioMayor := medalla.year;
            currentAnio := medalla.year;
            sedeMayor := BUSCADISCxCOD(medalla.intCod);
            contador := 2;
        end;
        // Si es el mismo a�o sumo
        if( currentAnio = anioMayor ) then
        begin
            medallasTemp := medalla.medallas[1] + medalla.medallas[2] + medalla.medallas[3];
        end;
        else 
        // Si cambia de a�o verifico si es mayor que lo que tenia guardado
        begin
            if( medallasTemp > medallasMayor ) then
            begin
                medallasMayor := medallasTemp;
                sedeMayor := medalla.BUSCADISCxCOD(medalla.intCod); 
                medallasTemp := 0; // Seteo en 0
                medallasTemp := medalla.medallas[1] + medalla.medallas[2] + medalla.medallas[3]; // Si es otro a�o y si es mayor ya guardo las nuevas medallas
            end;
            else
            // Si no es mayor vuelvo a 0 las medallas temporales
            begin 
                medallasTemp := 0;
            end; 
        end;
    end;
end;

begin
    opc:=0;
    repeat
    repeat
        writeln('BIENVENIDOS AL SISTEMA DE ADMINISTRACION DE LOS JUEGOS OLIMPICOS');
        writeln();
        writeln('Elija alguna de las opciones:');
        writeln('                      1- Alteracion de los registros.');
        writeln('                      2- Atletas olimpicos participantes por año.');
        writeln('                      3- Listado de medallas ganadas de cada tipo por año.');
        writeln('                      4- Listado de disciplinas en las que se participo por año.');
        writeln('                      5- Trayectoria por atleta.');
        writeln('                      6- Participantes por disciplina y año.');
        writeln('                      7- Pais sede donde se consiguio mayor cantidad de medallas.');
        writeln('                      8- TOP 10 deportistas con mayor cantidad de medallas.');
        writeln('                      9- Mostrar alguno de los 5 archivos.');
        writeln('                      10- Salir del programa.');
        readln(opc);
    until (opc >0) and (opc<11);
    ClrScr;
    case opc of
        1: ABM;
        2: ATLXANO;
        3: MEDXANO;
        4: DISXANO;
        5: TRAXATL;
        6: ATLXDIS;
        7: PAIXMED;
        //8: TOP10;
        9: MOSTRAR;
    end;
    until (opc = 10);
end.










